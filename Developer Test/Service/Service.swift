//
//  Service.swift
//  Developer Test
//
//  Created by Alen Sebalj on 01/03/2020.
//

import Foundation
import RxSwift

class Service: ServiceProtocol{
    
    func fetchData<T: Decodable>(request: RequestModel, memberType: T.Type ) -> Observable<T> {
        
        return Observable.create { (observer) -> Disposable in
            
            guard let requestUrl = request.urlRequest() else { return Disposables.create {} }
            
            let task = URLSession.shared.dataTask(with: requestUrl) { (data, response, error) in
                guard let data = data else {
                    if let error = error{
                        observer.onError(error)
                    }
                    return
                }
                
                do{
                    let objectData = try JSONDecoder().decode(T.self, from: data)
                    observer.onNext(objectData)
                    observer.onCompleted()
                } catch{
                    observer.onError(error)
                }
            }
            task.resume()
            return Disposables.create { task.cancel() }
        }
    }
}
