//
//  ServiceManager.swift
//  Developer Test
//
//  Created by Alen Sebalj on 14/11/2020.
//

import Foundation

class ServiceManager{
    
    static let shared = ServiceManager()
    
    let baseURL = "https://zpk2uivb1i.execute-api.us-east-1.amazonaws.com/dev/"
    
    let teachersApi = "teachers"
    let studentsApi = "students"
    let schoolDataApi = "schools"
}
