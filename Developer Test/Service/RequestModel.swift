//
//  RequestModel.swift
//  Developer Test
//
//  Created by Alen Sebalj on 14/11/2020.
//

import Foundation

enum RequestHTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

struct RequestModel {
    let path: String
    let parameters: [String: Any?]
    let headers: [String: String]
    let method: RequestHTTPMethod
}

extension RequestModel {
    
    func urlRequest() -> URLRequest? {
        let endpoint: String = ServiceManager.shared.baseURL.appending(path)
        
        guard var components = URLComponents(string: endpoint) else { return nil }
        
        components.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value as? String)
        }
        
        guard let url = components.url else { return nil }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        
        headers.forEach({
            urlRequest.addValue($0.value, forHTTPHeaderField: $0.key)
        })
        
        return urlRequest
    }
}
