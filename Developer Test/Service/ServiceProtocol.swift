//
//  ServiceProtocol.swift
//  Developer Test
//
//  Created by Alen Sebalj on 14/11/2020.
//

import Foundation
import RxSwift

protocol ServiceProtocol {
    func fetchData<T: Decodable>(request: RequestModel, memberType: T.Type ) -> Observable<T>
}
