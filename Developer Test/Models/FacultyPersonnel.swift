//
//  FacultyPersonnel.swift
//  Developer Test
//
//  Created by Alen Sebalj on 01/03/2020.
//

import UIKit

class FacultyPersonnel: Decodable {
    
    let id: Int
    let name: String
    let school_id: Int
    var school: School? = nil
    var detail: FacultyPersonnelDetail? = nil
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case school_id
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        school_id = try container.decode(Int.self, forKey: .school_id)
    }
}

struct FacultyPersonnelDetail: Decodable, Equatable{
    let id: Int
    let description: String
}
