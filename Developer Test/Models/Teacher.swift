//
//  Teacher.swift
//  Developer Test
//
//  Created by Alen Sebalj on 01/03/2020.
//

import UIKit

class Teacher: FacultyPersonnel {
    
    let department: String
    let image_url: String?
    
    private enum CodingKeys: String, CodingKey {
        case department = "class"
        case image_url
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        department = try container.decode(String.self, forKey: .department)
        image_url = try container.decodeIfPresent(String.self, forKey: .image_url)
        try super.init(from: decoder)
    }
    
}
