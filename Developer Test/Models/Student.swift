//
//  Student.swift
//  Developer Test
//
//  Created by Alen Sebalj on 01/03/2020.
//

import UIKit

class Student: FacultyPersonnel {
    
    let grade: Int
    
    private enum CodingKeys: String, CodingKey {
        case grade
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        grade = try container.decode(Int.self, forKey: .grade)
        try super.init(from: decoder)
    }
}
