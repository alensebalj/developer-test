//
//  School.swift
//  Developer Test
//
//  Created by Alen Sebalj on 02/03/2020.
//

import UIKit

class School: Decodable {
    let id: Int?
    let name: String?
    let image_url: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case image_url
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        image_url = try container.decodeIfPresent(String.self, forKey: .image_url)
    }
}
