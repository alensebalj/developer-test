//
//  PersonnelType.swift
//  Developer Test
//
//  Created by Alen Sebalj on 04/03/2020.
//

import Foundation

enum PersonnelType{
    case teacher
    case student
}
