//
//  LocalizationService.swift
//  Developer Test
//
//  Created by Alen Sebalj on 01/03/2020.
//

import Foundation

enum LocalizationKey: String{
    case teachers
    case students
    case teachers_dev
    case students_dev
    case classe = "class"
    case grade
    case contact
    case email
    case message
    case call
    case cancel
    case details
    
    var string: String{
        return rawValue.localized()
    }
}

extension LocalizationKey{
    static func getLocalizationKey(for string: String) -> LocalizationKey?{
        return LocalizationKey(rawValue: string)
    }
}

extension String{
    func localized() -> String {
      let localizedString = NSLocalizedString(self, comment: "")
      return localizedString
    }
}
