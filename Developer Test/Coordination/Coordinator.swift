//
//  Coordinator.swift
//  Developer Test
//
//  Created by Alen Sebalj on 15/11/2020.
//

import UIKit

protocol Coordinator {
    func start()
    func coordinate(to coordinator: Coordinator)
}

extension Coordinator {
    func coordinate(to coordinator: Coordinator) {
        coordinator.start()
    }
}
