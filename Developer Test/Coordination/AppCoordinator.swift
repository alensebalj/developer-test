//
//  AppCoordinator.swift
//  Developer Test
//
//  Created by Alen Sebalj on 14/11/2020.
//

import UIKit

class AppCoordinator: Coordinator{
    
    private let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        let rootViewControllerCoordinator = MainTabBarCoordinator()
        coordinate(to: rootViewControllerCoordinator)
        window.rootViewController = rootViewControllerCoordinator.tabBarController
        window.makeKeyAndVisible()
    }
}
