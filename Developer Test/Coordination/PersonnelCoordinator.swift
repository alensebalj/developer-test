//
//  PersonnelCoordinator.swift
//  Developer Test
//
//  Created by Alen Sebalj on 15/11/2020.
//

import UIKit

protocol PersonnelFlow{
    func coordinateToDetails(personnelViewModel: FacultyPersonnelViewModel, personnelType: PersonnelType)
}

class PersonnelCoordinator: Coordinator, PersonnelFlow{
    weak var navigationController: UINavigationController?
    private var personnelType: PersonnelType
    
    init(navigationController: UINavigationController?, personnelType: PersonnelType) {
        self.navigationController = navigationController
        self.personnelType = personnelType
    }
    
    func start() {
        let personnelViewController = PersonnelViewController(personnelType: personnelType)
        personnelViewController.coordinator = self
        navigationController?.pushViewController(personnelViewController, animated: true)
        navigationController?.setBigTitlesNavigationController()
    }
    
    func coordinateToDetails(personnelViewModel: FacultyPersonnelViewModel, personnelType: PersonnelType) {
        
        guard let navigationController = navigationController else { return }
        let personnelDetailsCoordinator = PersonnelDetailsCoordinator(navigationController: navigationController, personnelViewModel: personnelViewModel, personnelType: personnelType)
        
        coordinate(to: personnelDetailsCoordinator)
    }
}
