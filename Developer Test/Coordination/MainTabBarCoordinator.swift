//
//  MainTabBarCoordinator.swift
//  Developer Test
//
//  Created by Alen Sebalj on 16/11/2020.
//

import UIKit

class MainTabBarCoordinator: Coordinator{
    
    let tabBarController: MainTabBarViewController?
    
    private enum TabBarIconType: String, CaseIterable{
        case teachers
        case students
    }
    
    init() {
        tabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as? MainTabBarViewController
    }
    
    func start() {
        tabBarController?.coordinator = self
        
        let teachersNavigationViewController = UINavigationController()
        let teachersCoordinator = PersonnelCoordinator(navigationController: teachersNavigationViewController, personnelType: .teacher)
        
        let studentsNavigationViewController = UINavigationController()
        let studentsCoordinator = PersonnelCoordinator(navigationController: studentsNavigationViewController, personnelType: .student)
        
        self.tabBarController?.viewControllers = [teachersNavigationViewController, studentsNavigationViewController]
        
        setViewControllerDetails()
        
        coordinate(to: teachersCoordinator)
        coordinate(to: studentsCoordinator)
    }
    
    private func setViewControllerDetails(){
        
        guard let viewControllers = tabBarController?.viewControllers else { return }
        
        for (i, controller) in viewControllers.enumerated(){
            controller.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "\(TabBarIconType.allCases[i].rawValue)_icon"), tag: i)
            
            #if STAGING
            tabBarController?.tabBar.items?[i].title = LocalizationKey.getLocalizationKey(for: "\(TabBarIconType.allCases[i].rawValue)_dev")?.string
            #else
            tabBarController?.tabBar.items?[i].title = LocalizationKey.getLocalizationKey(for: TabBarIconType.allCases[i].rawValue)?.string
            #endif
        }
    }
}
