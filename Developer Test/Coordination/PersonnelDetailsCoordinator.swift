//
//  PersonnelDetailsCoordinator.swift
//  Developer Test
//
//  Created by Alen Sebalj on 15/11/2020.
//

import UIKit

class PersonnelDetailsCoordinator: Coordinator {
    weak var navigationController: UINavigationController?
    private var personnelViewModel: FacultyPersonnelViewModel
    private var personnelType: PersonnelType
    
    init(navigationController: UINavigationController, personnelViewModel: FacultyPersonnelViewModel, personnelType: PersonnelType) {
        self.navigationController = navigationController
        self.personnelViewModel = personnelViewModel
        self.personnelType = personnelType
    }
    
    func start() {
        let personnelDetailsViewController = PersonnelDetailsViewController(personnelViewModel: personnelViewModel, personnelType: personnelType)
        personnelDetailsViewController.coordinator = self
        navigationController?.pushViewController(personnelDetailsViewController, animated: true)
    }
}
