//
//  PersonnelDetailsHeaderView.swift
//  Developer Test
//
//  Created by Alen Sebalj on 02/03/2020.
//

import UIKit
import Kingfisher

class PersonnelDetailsHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var classLabelOutlet: UILabel!
    @IBOutlet weak var schoolImageView: UIImageView!
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var personnelNameLabel: UILabel!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var schoolLabel: UILabel!

    var personnelViewModel: FacultyPersonnelViewModel? {
        didSet{
            setUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setBackground(toColor: .white)
        contactButton.setOvalShape11()
    }
    
    private func setUI(){
        personnelNameLabel.text = personnelViewModel?.name
        schoolLabel.text = personnelViewModel?.schoolName
        
        guard let personnelViewModel = personnelViewModel else {
            return
        }
        
        switch personnelViewModel{
        
        case let personnelViewModel as TeacherViewModel:
            classLabelOutlet.text = LocalizationKey.classe.string
            classLabel.text = personnelViewModel.department
            
        case let personnelViewModel as StudentViewModel:
            classLabelOutlet.text = LocalizationKey.grade.string
            contactButton.isHidden = true
            classLabel.text = personnelViewModel.grade
            
        default:
            break
        }
        
        if let imageUrl = personnelViewModel.schoolImageUrl{
            schoolImageView.kf.setImage(with: URL(string: imageUrl))
        }
    }
    
}
