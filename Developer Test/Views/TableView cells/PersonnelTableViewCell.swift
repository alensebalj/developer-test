//
//  PersonnelTableViewCell.swift
//  Developer Test
//
//  Created by Alen Sebalj on 02/03/2020.
//

import UIKit
import Kingfisher

class PersonnelTableViewCell: UITableViewCell {
    
    @IBOutlet weak var classLabelOutlet: UILabel!
    @IBOutlet weak var personnelImageView: UIImageView!
    @IBOutlet weak var layerView: UIView!
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var personnelNameLabel: UILabel!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var schoolLabel: UILabel!
    @IBOutlet weak var contactButtonHeightConstraint: NSLayoutConstraint!
    
    var personnelViewModel:FacultyPersonnelViewModel? {
        didSet{
            setUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setCellLayout()
    }
    
    private func setUI(){
        personnelNameLabel.text = personnelViewModel?.name
        schoolLabel.text = personnelViewModel?.schoolName
        
        switch personnelViewModel{
        
        case let personnelViewModel as TeacherViewModel:
            classLabelOutlet.text = LocalizationKey.classe.string
            classLabel.text = personnelViewModel.department
            if let imageURL = URL(string: personnelViewModel.imageUrl?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""){
                personnelImageView.kf.setImage(with: imageURL)
            }
            
        case let personnelViewModel as StudentViewModel:
            classLabelOutlet.text = LocalizationKey.grade.string
            classLabel.text = personnelViewModel.grade
            personnelImageView.image = UIImage(named: "students_icon")
            
        default:
            break
        }
    }
    
    private func setCellLayout(){
        layerView.setOvalShape15()
        contactButton.setOvalShape11()
        personnelImageView.setRoundShape()
    }
}
