//
//  PersonnelDetailsTableViewCell.swift
//  Developer Test
//
//  Created by Alen Sebalj on 03/03/2020.
//

import UIKit

class PersonnelDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    
    var personnelViewModel:FacultyPersonnelViewModel? {
        didSet{
            setUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    private func setUI(){
        descriptionLabel.text = personnelViewModel?.details
    }
}
