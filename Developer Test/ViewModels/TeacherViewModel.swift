//
//  TeacherViewModel.swift
//  Developer Test
//
//  Created by Alen Sebalj on 14/11/2020.
//

import Foundation

class TeacherViewModel: FacultyPersonnelViewModel{
    
    var department: String {
        guard let personnel = personnel as? Teacher else { return "" }
        return personnel.department
    }
    
    var imageUrl: String?{
        guard let personnel = personnel as? Teacher else { return "" }
        return personnel.image_url
    }
    
    init(teacher: Teacher) {
        super.init(personnel: teacher)
    }

}
