//
//  StudentViewModel.swift
//  Developer Test
//
//  Created by Alen Sebalj on 14/11/2020.
//

import Foundation

class StudentViewModel: FacultyPersonnelViewModel{
    
    var grade: String {
        guard let personnel = personnel as? Student else { return "" }
        return String(personnel.grade)
    }
    
    init(student: Student) {
        super.init(personnel: student)
    }
}
