//
//  FacultyPersonnelViewModel.swift
//  Developer Test
//
//  Created by Alen Sebalj on 14/11/2020.
//

import Foundation

class FacultyPersonnelViewModel{
    
    let personnel: FacultyPersonnel
    
    var name: String {
        return personnel.name
    }
    
    var schoolName: String? {
        return personnel.school?.name
    }
    
    var schoolImageUrl: String? {
        return personnel.school?.image_url
    }
    
    var details: String? {
        return personnel.detail?.description
    }
    
    init(personnel: FacultyPersonnel) {
        self.personnel = personnel
    }
}
