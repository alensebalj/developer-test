//
//  UINavigationControllerExtension.swift
//  Developer Test
//
//  Created by Alen Sebalj on 22.11.2020..
//

import UIKit

extension UINavigationController{
    func setBigTitlesNavigationController() {
        self.navigationBar.prefersLargeTitles = true
    }
}
