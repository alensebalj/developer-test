//
//  ExtensionHelpers.swift
//  Developer Test
//
//  Created by Alen Sebalj on 02/03/2020.
//

import UIKit

extension UIAlertController{
    static func createAlert(title: String, message: String?, style:UIAlertController.Style, actions: [UIAlertAction]) -> UIAlertController{
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        for action in actions{
            alertController.addAction(action)
        }
        return alertController
    }
    
    static func createContactPersonDialog(presentIn viewController: UIViewController){
        let alertController = UIAlertController.createAlert(title: LocalizationKey.contact.string, message: nil, style: .actionSheet, actions: [
            UIAlertAction(title: LocalizationKey.email.string, style: .default, handler: nil),
            UIAlertAction(title: LocalizationKey.message.string, style: .default, handler: nil),
            UIAlertAction(title: LocalizationKey.call.string, style: .default, handler: nil),
            UIAlertAction(title: LocalizationKey.cancel.string, style: .cancel, handler: nil)
        ])
        
        if let popoverController = alertController.popoverPresentationController{
            popoverController.sourceView = viewController.view
            popoverController.sourceRect = CGRect(x: viewController.view.bounds.midX, y: viewController.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        viewController.present(alertController, animated: true, completion: nil)
    }
}
