//
//  ExtensionHelpers.swift
//  Developer Test
//
//  Created by Alen Sebalj on 02/03/2020.
//

import Foundation
import UIKit

extension UIView{
    func setOvalShape11(){
        self.layer.cornerRadius = self.bounds.width/11
    }
    func setRoundShape(){
        self.layer.cornerRadius = self.bounds.width/2
    }
    func setOvalShape15(){
        self.layer.cornerRadius = self.bounds.width/15
    }
}
