//
//  TableHeaderFooterViewBackground.swift
//  Developer Test
//
//  Created by Alen Sebalj on 16/11/2020.
//

import UIKit

extension UITableViewHeaderFooterView{
    func setBackground(toColor color: UIColor){
        let customView = UIView(frame: self.bounds)
        customView.backgroundColor = color
        backgroundView = customView
    }
}
