//
//  PersonnelViewModelList.swift
//  Developer Test
//
//  Created by Alen Sebalj on 14/11/2020.
//

import Foundation
import RxSwift
import RxRelay

final class PersonnelViewModelList{
    
    var title: String?
    var personnel: Observable<[FacultyPersonnelViewModel]>?
    
    private let service: ServiceProtocol
    private var personnelType: PersonnelType
    private var disposeBag = DisposeBag()
    
    init(personnelType: PersonnelType, service: ServiceProtocol = Service()) {
        self.personnelType = personnelType
        self.service = service
        setControllerTitle()
        fetchPersonnelViewModels()
    }
    
    private func setControllerTitle(){
        #if STAGING
            personnelType == .teacher ? (title = LocalizationKey.teachers_dev.string) : (title = LocalizationKey.students_dev.string)
        #else
            personnelType == .teacher ? (title = LocalizationKey.teachers.string) : (title = LocalizationKey.students.string)
        #endif
    }
    
    private func fetchPersonnelViewModels(){
        switch personnelType{
        case .teacher:
            let requestModel = RequestModel(path: ServiceManager.shared.teachersApi, parameters: [:], headers: [:], method: .get)
            
            let teachers = service.fetchData(request: requestModel, memberType: [Teacher].self).share(replay: 1)
            
            let schools = teachers.flatMap { (teachers) -> Observable<[School]> in
                
                let school = teachers.map { (teacher) -> Observable<School> in
                    return self.fetchSchoolData(facultyPersonnel: teacher)
                }
                return Observable.zip(school)
            }
            
            personnel = Observable.zip(teachers, schools).map { (teachers, schools) in
                teachers.enumerated().forEach({ index, teacher in
                    teacher.school = schools[index]
                })
                
                return teachers.map({ TeacherViewModel(teacher: $0) })
            }

        
        case .student:
            let requestModel = RequestModel(path: ServiceManager.shared.studentsApi, parameters: [:], headers: [:], method: .get)
            
            let students = service.fetchData(request: requestModel, memberType: [Student].self).share(replay: 1)
            
            let schools = students.flatMap { (students) -> Observable<[School]> in
                
                let school = students.map { (student) -> Observable<School> in
                    return self.fetchSchoolData(facultyPersonnel: student)
                }
                return Observable.zip(school)
            }
            
            personnel = Observable.zip(students, schools).map { (students, schools) in
                students.enumerated().forEach({ index, student in
                    student.school = schools[index]
                })
                
                return students.map({ StudentViewModel(student: $0) })
            }
        }
    }

    private func fetchSchoolData(facultyPersonnel: FacultyPersonnel) -> Observable<School>{
        let schoolRequestModel = RequestModel(path: ServiceManager.shared.schoolDataApi + "/\(facultyPersonnel.school_id)", parameters: [:], headers: [:], method: .get)
        return self.service.fetchData(request: schoolRequestModel, memberType: School.self)
    }
}
