//
//  PersonnelDetailsViewModelList.swift
//  Developer Test
//
//  Created by Alen Sebalj on 15/11/2020.
//

import Foundation
import RxSwift
import RxRelay

final class PersonnelDetailsViewModelList{
    var title: String?
    var facultyMember: BehaviorRelay<[FacultyPersonnelViewModel]>
    
    private let service: ServiceProtocol
    private var personnelType: PersonnelType
    private var disposeBag = DisposeBag()
    private var personnelViewModel: FacultyPersonnelViewModel
    
    init(personnelType: PersonnelType, personnelViewModel: FacultyPersonnelViewModel, service: ServiceProtocol = Service()) {
        self.personnelType = personnelType
        self.service = service
        self.facultyMember = BehaviorRelay(value: [])
        self.personnelViewModel = personnelViewModel
        setControllerTitle()
        fetchPersonnelDetails()
    }
    
    private func setControllerTitle(){
        title = LocalizationKey.details.string
    }
    
    private func fetchPersonnelDetails(){
        
        let requestModel = RequestModel(path: "\(personnelType == .teacher ? ServiceManager.shared.teachersApi : ServiceManager.shared.studentsApi)/\(personnelViewModel.personnel.id)", parameters: [:], headers: [:], method: .get)
        
        service.fetchData(request: requestModel, memberType: FacultyPersonnelDetail.self).map({ [weak self] in
            if let self = self{
                self.personnelViewModel.personnel.detail = $0
                self.facultyMember.accept([self.personnelViewModel])
            }
        })
        .subscribe()
        .disposed(by: disposeBag)

    }
}
