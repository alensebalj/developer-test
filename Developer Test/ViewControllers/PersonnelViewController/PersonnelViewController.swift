//
//  PersonnelViewController.swift
//  Developer Test
//
//  Created by Alen Sebalj on 01/03/2020.
//

import UIKit
import RxSwift
import RxCocoa

class PersonnelViewController: UIViewController {
    
    @IBOutlet weak var personnelTableView: UITableView!
    
    var coordinator: PersonnelCoordinator?
    private let personnelType: PersonnelType
    private let disposeBag = DisposeBag()
    private let personnelViewModelList: PersonnelViewModelList
    
    init(personnelType: PersonnelType) {
        self.personnelType = personnelType
        personnelViewModelList = PersonnelViewModelList(personnelType: personnelType)
        super.init(nibName: "PersonnelViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle()
        setTableView()
        setPersonnelDataSource()
        setOnClickEvent()
    }
    
    private func setNavigationTitle(){
        navigationItem.title = personnelViewModelList.title
    }
    
    private func setTableView(){
        personnelTableView.register(UINib(nibName: "PersonnelTableViewCell", bundle: nil), forCellReuseIdentifier: "personnelCell")
    }
    
    private func setPersonnelDataSource(){
        personnelViewModelList.personnel?
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: personnelTableView.rx.items(cellIdentifier: "personnelCell", cellType: PersonnelTableViewCell.self)) { [weak self] index, viewModel, cell in
                guard let strongSelf = self else { return }
                cell.personnelViewModel = viewModel
                cell.contactButton.rx.tap.bind {
                    strongSelf.contactPersonnel()
                }.disposed(by: strongSelf.disposeBag)
            }.disposed(by: disposeBag)
    }
    
    private func setOnClickEvent(){
        personnelTableView.rx
            .modelSelected(FacultyPersonnelViewModel.self)
            .subscribe (onNext: { [weak self] personnelViewModel in
                if let self = self{
                    self.coordinator?.coordinateToDetails(personnelViewModel: personnelViewModel, personnelType: self.personnelType)
                }
        }).disposed(by: disposeBag)
    }
    
    private func contactPersonnel(){
        UIAlertController.createContactPersonDialog(presentIn: self)
    }
}
