//
//  PersonnelDetailsViewController.swift
//  Developer Test
//
//  Created by Alen Sebalj on 02/03/2020.
//

import UIKit
import RxSwift
import RxCocoa

class PersonnelDetailsViewController: UIViewController {
    
    @IBOutlet weak var personnelDetailsTableView: UITableView!

    var coordinator: PersonnelDetailsCoordinator?
    
    private let personnelViewModel: FacultyPersonnelViewModel
    private let personnelType: PersonnelType
    private let disposeBag = DisposeBag()
    private let personnelDetailsViewModelList: PersonnelDetailsViewModelList
    
    init(personnelViewModel: FacultyPersonnelViewModel, personnelType: PersonnelType) {
        self.personnelViewModel = personnelViewModel
        self.personnelType = personnelType
        self.personnelDetailsViewModelList = PersonnelDetailsViewModelList(personnelType: personnelType, personnelViewModel: personnelViewModel)
        super.init(nibName: "PersonnelDetailsViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle()
        setTableView()
        setPersonnelDetailsDataSource()
    }
    
    private func setNavigationTitle(){
        navigationItem.title = personnelDetailsViewModelList.title
    }
    
    private func setTableView(){
        personnelDetailsTableView.register(UINib(nibName: "PersonnelDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "personnelDetailsCell")
        personnelDetailsTableView.register(UINib(nibName: "PersonnelDetailsHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "personnelDetailsHeader")
        personnelDetailsTableView.sectionHeaderHeight = UITableView.automaticDimension
        personnelDetailsTableView.estimatedSectionHeaderHeight = 275
        
        personnelDetailsTableView
            .rx.setDelegate(self)
            .disposed(by: disposeBag)
    }

    private func setPersonnelDetailsDataSource(){
        personnelDetailsViewModelList.facultyMember
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: personnelDetailsTableView.rx.items(cellIdentifier: "personnelDetailsCell", cellType: PersonnelDetailsTableViewCell.self)) { index, viewModel, cell in
                cell.personnelViewModel = viewModel
            }.disposed(by: disposeBag)
    }
    
    private func contactPersonnel(){
        UIAlertController.createContactPersonDialog(presentIn: self)
    }
}

extension PersonnelDetailsViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = personnelDetailsTableView.dequeueReusableHeaderFooterView(withIdentifier: "personnelDetailsHeader") as? PersonnelDetailsHeaderView else { return nil}
        headerView.personnelViewModel = personnelViewModel
        headerView.contactButton.rx.tap.bind{ [weak self] in
            self?.contactPersonnel()
        }.disposed(by: disposeBag)
        return headerView
    }
    
}
